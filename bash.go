package main

import (
	"errors"
	"fmt"
	"log"
	"os/exec"
	"strings"
	"sync"
)

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

var (
	errorTooLong   = errors.New("Input too long (>65535 chars)")
	errorReaderNil = errors.New("Reader is nil")
)

type nosjCommit struct {
	sync.RWMutex
	Commit string
}

func newnosj() *nosjCommit {
	log.Println("Initializing nosj")
	return &nosjCommit{}
}

func (c *nosjCommit) execnosj(input string) (r []byte, e error) {
	if len(input) > 65535 {
		e = errorTooLong
		return
	}

	reader := strings.NewReader(input)
	if reader == nil {
		e = errorReaderNil
		return
	}

	c.RLock()
	defer c.RUnlock()

	cmd := exec.Command("/bin/bash", fmt.Sprintf(
		`/tmp/%s.sh`, c.Commit,
	))

	cmd.Stdin = reader

	stdout, err := cmd.Output()
	if err != nil {
		e = err
		return
	}

	return stdout, nil
}
