package main

import (
	"strings"

	"github.com/alecthomas/chroma/formatters/html"
	"github.com/alecthomas/chroma/lexers"
	"github.com/alecthomas/chroma/styles"
)

func highlight(code, lang string) string {
	var lexer = lexers.Get(lang)
	if lexer == nil {
		lexer = lexers.Fallback
	}

	// Set a syntax highlighting theme
	style := styles.Get("github")
	if style == nil {
		style = styles.Fallback
	}

	iterator, err := lexer.Tokenise(nil, code)
	if err != nil {
		return code
	}

	formatter := html.New()
	builder := &strings.Builder{}

	err = formatter.Format(builder, style, iterator)
	if err != nil {
		return code
	}

	return builder.String()
}
