package main

import (
	"errors"
	"log"
	"net/http"
	"text/template"
	"time"

	"github.com/gobuffalo/packr"
	"github.com/valyala/fastjson"
)

const (
	addr   = ":8080"
	sample = `{
    "menu": {
        "id": "file",
        "value": "File",
        "popup": {
            "menuitem": [
                {
                    "value": "New",
                    "onclick": "CreateNewDoc"
                },
                {
                    "value": "Open",
                    "onclick": "OpenDoc"
                },
                {
                    "value": "Close",
                    "onclick": "CloseDoc"
                }
            ]
        }
    }
}`
)

var (
	jsp             fastjson.Parser
	updateFrequency = time.Minute * 2

	errorEmptyJSON = errors.New("Empty JSON given")

	box = packr.NewBox("./")
)

type data struct {
	Commit string
	Input  string
	Output string
}

func main() {
	html, err := box.FindString("index.html")
	if err != nil {
		panic(err)
	}

	index := template.Must(template.New("index").Parse(html))

	nosj := newnosj()
	if err := nosj.updatenosj(); err != nil {
		panic(err)
	}

	go func() {
		tr := time.NewTicker(
			time.Duration(updateFrequency),
		)

		for {
			<-tr.C
			if err := nosj.updatenosj(); err != nil {
				log.Println(err)
			}
		}
	}()

	defer nosj.cleanup()

	http.HandleFunc("/",
		func(w http.ResponseWriter, r *http.Request) {
			switch r.Method {
			case "GET":
				d := data{
					Input:  sample,
					Commit: nosj.Commit[:8],
				}

				index.Execute(w, d)
			case "POST":
				body := r.PostFormValue("json")
				if body == "" {
					errhttp(w, errorEmptyJSON)
					return
				}

				resp, err := nosj.execnosj(body)
				if err != nil {
					errhttp(w, err)
					return
				}

				d := data{
					Commit: nosj.Commit[:8],
					Input:  body,
					Output: highlight(string(resp), "sh"),
				}

				index.Execute(w, d)
			default:
				w.WriteHeader(401)
				w.Write([]byte("Needs GET/POST"))
			}
		},
	)

	log.Printf("Listening to %s\n", addr)

	if err := http.ListenAndServe(addr, nil); err != nil {
		panic(err)
	}
}

func errhttp(w http.ResponseWriter, err error) {
	w.WriteHeader(500)
	w.Write([]byte(err.Error()))
}
