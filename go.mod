module gitlab.com/diamondburned/nosj-web

require (
	github.com/alecthomas/chroma v0.6.2
	github.com/andreyvit/diff v0.0.0-20170406064948-c7f18ee00883
	github.com/gobuffalo/packr v1.22.0
	github.com/valyala/fastjson v1.4.0
)
