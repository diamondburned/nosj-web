package main

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/valyala/fastjson"
)

var errorCommitHash = errors.New("Failed parsing latest commit")

func (oldcommit *nosjCommit) updatenosj() (err error) {
	log.Println("Updating nosj")
	api, err := http.Get("https://api.github.com/repos/dylanaraps/nosj/commits")
	if err != nil {
		return
	}

	defer api.Body.Close()

	body, err := ioutil.ReadAll(api.Body)
	if err != nil {
		return
	}

	commithash := fastjson.GetString(body, "0", "sha")

	if commithash == "" {
		err = errorCommitHash
		return
	}

	if commithash == oldcommit.Commit {
		return nil
	}

	log.Printf("Found nosj commit %s, updating\n", commithash)

	oldcommit.Lock()
	defer oldcommit.Unlock()

	oldcommit.cleanup()
	oldcommit.Commit = (commithash)

	resp, err := http.Get("https://raw.githubusercontent.com/dylanaraps/nosj/master/nosj")
	if err != nil {
		return
	}

	defer resp.Body.Close()

	file, err := os.OpenFile(
		fmt.Sprintf(`/tmp/%s.sh`, commithash),
		os.O_RDWR|os.O_TRUNC|os.O_SYNC|os.O_CREATE,
		os.ModePerm,
	)

	if err != nil {
		return
	}

	defer file.Close()

	_, err = io.Copy(file, resp.Body)
	if err != nil {
		return
	}

	log.Println("Updated successfully")

	return nil
}

func (oldcommit *nosjCommit) cleanup() error {
	return os.Remove(fmt.Sprintf(
		"/tmp/%s.sh", oldcommit.Commit,
	))
}
