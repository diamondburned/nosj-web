package main

import (
	"testing"

	"github.com/andreyvit/diff"
)

const json = `{"menu": {
  "id": "file",
  "value": "File",
  "popup": {
    "menuitem": [
      {"value": "New", "onclick": "CreateNewDoc()"},
      {"value": "Open", "onclick": "OpenDoc()"},
      {"value": "Close", "onclick": "CloseDoc()"}
    ]
  }
}}`

const results = `nosj[menu.popup.menuitem.value[1]]=Open
nosj[menu.id]=file
nosj[menu.value]=File
nosj[menu.popup.menuitem.onclick[1]]=OpenDoc\(\)
nosj[menu.popup.menuitem.value[2]]=Close
nosj[menu.popup.menuitem.onclick[0]]=CreateNewDoc\(\)
nosj[menu.popup.menuitem.onclick]=CreateNewDoc\(\)
nosj[menu.popup.menuitem.value[0]]=New
nosj[menu.popup.menuitem.onclick[2]]=CloseDoc\(\)
nosj[menu.popup.menuitem.value]=New
`

func TestExec(t *testing.T) {
	nosj := newnosj()
	if err := nosj.updatenosj(); err != nil {
		t.Fatal(err)
	}

	resp, err := nosj.execnosj(json)
	if err != nil {
		t.Fatal(err)
	}

	if string(resp) != results {
		t.Errorf("Test failed---\n%v", diff.LineDiff(
			string(resp), results,
		))
	}
}
